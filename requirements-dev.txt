confluent-kafka>=2.0.0
coverage
flake8==3.7.7
hop-client>=0.7.0
ipdb
ipython
pytest
tox
